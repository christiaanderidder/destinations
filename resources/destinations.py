from flask_restful import Resource, reqparse, abort


class DestinationCollection(Resource):
    def __init__(self, db):
        self.db = db
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', help='Destination name is required', required=True, location='json')
        self.parser.add_argument('lat', type=float, help='The destinations lat coordinate is required', required=True, location='json')
        self.parser.add_argument('lon', type=float, help='The destinations lon coordinate is required', required=True, location='json')

    def get(self):
        res = self.db.query_db('select * from destinations')
        return res

    def post(self):
        args = self.parser.parse_args()
        self.db.query_db('insert into destinations (name, lat, lon) values(?, ?, ?)', [args['name'], args['lat'], args['lon']])
        res = self.get()
        return res, 201


class Destination(Resource):
    def __init__(self, db):
        self.db = db
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', help='Destination name is required', required=True, location='json')
        self.parser.add_argument('lat', type=float, help='The destinations lat coordinate is required', required=True, location='json')
        self.parser.add_argument('lon', type=float, help='The destinations lon coordinate is required', required=True, location='json')

    def get(self, dest_id):
        res = self.db.query_db('select * from destinations where id = ?', [dest_id], single=True)
	if not res:
	        abort(404, description='No destination found with ID %s' % dest_id)
        return res

    def put(self, dest_id):
        args = self.parser.parse_args()
        self.db.query_db('update destinations set name = ?, lat = ?, lon = ? where id = ?', [args['name'], args['lat'], args['lon'], dest_id])
        res = self.get(dest_id)
        return res, 201

    def delete(self, dest_id):
        self.get(dest_id)
        self.db.query_db('delete from destinations where id = ?', [dest_id])
        return '', 204

