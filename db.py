from flask import g
from os.path import isfile
import sqlite3


class Sqlite:

    def __init__(self, app):
        self.path = './storage/database.db'
        self.schema = './schema.sql'
        self.app = app

    def init_db(self):
        """DB Already exists, do not init"""
        if isfile(self.path):
            return

        with self.app.app_context():
            db = self.get_db()
            with self.app.open_resource(self.schema, mode='r') as f:
                db.cursor().executescript(f.read())
            db.commit()

    def get_db(self):
        db = getattr(g, '_database', None)
        if db is None:
            db = g._database = sqlite3.connect(self.path)
            db.row_factory = self.map_dict
        return db

    def query_db(self, query, args=(), single=False, scalar=False):
        print(query)
        print(args)
        db = self.get_db()
        with db:
            cur = db.execute(query, args)
            rv = cur.fetchall()
            cur.close()

        if scalar:
            return cur.lastrowid
        elif single:
            return rv[0] if rv else None
        else:
            return rv


    def close_db(self):
        db = self.get_db()
        if db is not None:
            db.close()

    def map_dict(self, cursor, row):
        return dict((cursor.description[idx][0], value)
                    for idx, value in enumerate(row))
