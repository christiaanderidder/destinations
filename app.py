from flask import Flask
from flask_restful import Api

from db import Sqlite
from resources.destinations import Destination, DestinationCollection

app = Flask(__name__)
api = Api(app)
db = Sqlite(app)

db.init_db()

api.add_resource(DestinationCollection, '/destinations', resource_class_kwargs={'db': db})
api.add_resource(Destination, '/destinations/<int:dest_id>', resource_class_kwargs={'db': db})

@app.teardown_appcontext
def teardown(exception):
    db.close_db()

if __name__ == '__main__':
    app.run(debug=True)
